import { TestBed } from '@angular/core/testing';

import { PokeapiService } from './pokeapi.service';
import {HttpClientModule} from '@angular/common/http';

describe('PokeapiService', () => {
  let service: PokeapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });

    service = TestBed.inject(PokeapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have some pokemon', (done) => {
    service.getCollection().subscribe((collection) => {
      expect(collection.count).toBeGreaterThan(0);
      done();
    });
  });

  it('should get pikachu by id', (done) => {
    service.get(25).subscribe((pokemon) => {
      expect(pokemon.name).toEqual('pikachu');
      expect(pokemon.id).toEqual(25);
      done();
    });
  });

  it('should get pikachu by name', (done) => {
    service.get('pikachu').subscribe((pokemon) => {
      expect(pokemon.name).toEqual('pikachu');
      expect(pokemon.id).toEqual(25);
      done();
    });
  });

  it('should extract pokemon ID', () => {
    expect(() => {
      service.extractId({
        name: 'pikachu',
        url: '',
      });
    }).toThrow(new Error('Invalid URL'));

    expect(service.extractId({
      name: 'pikachu',
      url: 'https://pokeapi.co/api/v2/pokemon/25/',
    })).toEqual('25');
  });
});
