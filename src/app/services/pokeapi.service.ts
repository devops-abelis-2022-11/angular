import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PokemonCollection} from '../model/pokemon-collection';
import {Observable} from 'rxjs';
import {Pokemon} from '../model/pokemon';
import {PokemonLight} from '../model/pokemon-light';

@Injectable({
  providedIn: 'root'
})
export class PokeapiService {
  constructor(
    private httpClient: HttpClient,
  ) { }

  public getCollection(page: number = 1, perPage: number = 20): Observable<PokemonCollection> {
    if (page < 1) {
      throw new Error('Page number cannot be lesser then 1');
    }

    if (perPage < 1) {
      throw new Error('Page number cannot be lesser then 10');
    }

    if (perPage > 50) {
      throw new Error('Page number cannot be greater then 50');
    }

    const offset = perPage * (page - 1);
    return this.httpClient.get<PokemonCollection>(`https://pokeapi.co/api/v2/pokemon?limit=${perPage}&offset=${offset}`);
  }

  public get(id: number): Observable<Pokemon>;
  public get(name: string): Observable<Pokemon>;
  public get(ask: number|string): Observable<Pokemon> {
    return this.httpClient.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${ask}`);
  }

  public extractId(pokemon: PokemonLight): string {
    const result = pokemon.url.match(/^https:\/\/pokeapi.co\/api\/v2\/pokemon\/(\d+|\w)\/?$/);

    if (null === result) {
      throw new Error('Invalid URL');
    }

    return result[1];
  }

  public getPokemonImageUrl(id: number|string): string {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
  }
}
