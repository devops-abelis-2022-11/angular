import {PokemonLight} from './pokemon-light';

export interface PokemonCollection {
  count: number;
  next: string|null;
  previous: string|null;
  results: Array<PokemonLight>;
}
