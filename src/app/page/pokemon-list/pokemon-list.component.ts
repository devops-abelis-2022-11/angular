import { Component, OnInit } from '@angular/core';
import {PokeapiService} from '../../services/pokeapi.service';
import {PokemonLight} from '../../model/pokemon-light';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  public pokemons: Array<PokemonLight> = [];
  public loading: boolean = false;
  private currentLoadedPage = 1;

  constructor(
    public pokeapiService: PokeapiService,
  ) { }

  ngOnInit(): void {
    this.nextLoad();
  }

  public nextLoad(): void {
    this.loading = true;
    this.pokeapiService.getCollection(this.currentLoadedPage, 40).subscribe((collection) => {
      this.pokemons = this.pokemons.concat(collection.results);
      this.currentLoadedPage++;
      this.loading = false;
    });
  }
}
